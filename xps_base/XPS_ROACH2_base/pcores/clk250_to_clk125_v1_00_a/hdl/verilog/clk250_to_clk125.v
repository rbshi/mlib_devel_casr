//-----------------------------------------------------------------------------
// Title         : 250Mhz Clk to 125Mhz Clk data interface
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : clk250_to_clk125.v
// Author        : Runbin Shi
// Created       : 21.11.2016
// Last modified : 23.11.2016
//-----------------------------------------------------------------------------
// Description :
//
//-----------------------------------------------------------------------------

module clk250_to_clk125(
                        input wire         clk_250,
                        input wire         clk_125,
                        input wire [127:0] data_in,
                        input wire         data_in_vld,
                        input wire         rst_n,

                        output reg [255:0] data_out,
                        output reg         data_out_vld
                        );


   reg                                     toggle_flag;
   reg                                     toggle_flag_d1;

   (* ASYNC_REG = "TRUE" *) reg [127:0]                             data_out_h128;
   (* ASYNC_REG = "TRUE" *) reg [127:0]                             data_out_l128;

   wire [255:0]                             data_out_d0;
   (* ASYNC_REG = "TRUE" *) reg [255:0]                             data_out_d1;
   (* ASYNC_REG = "TRUE" *) reg [255:0]                             data_out_d2;

   reg                                     data_out_vld_d0;
   reg                                     data_out_vld_d1;
   reg                                     data_out_vld_d2;


   always @(posedge clk_125) begin
      case(toggle_flag)
        1: begin
           data_out <= data_out_d1;
           data_out_vld <= data_out_vld_d1;
        end
        0: begin
           data_out <= data_out_d2;
           data_out_vld <= data_out_vld_d2;
        end
      endcase // case (toggle_flag)
   end

   always @(posedge clk_250) begin
      if(~rst_n) begin
         data_out_vld_d0 <= 0;
      end else begin
         case (toggle_flag & (~toggle_flag_d1))
           1: data_out_vld_d0 <= 1;
           default: data_out_vld_d0 <= 0;
         endcase // case (toggle_flag & (~toggle_flag_d1))
      end
   end

   always @(posedge clk_250) begin
      data_out_vld_d1 <= data_out_vld_d0;
      data_out_vld_d2 <= data_out_vld_d1;
      data_out_d1 <= data_out_d0;
      data_out_d2 <= data_out_d1;
   end


   always @(posedge clk_250) begin
		case (toggle_flag)
		  1: data_out_h128 <= data_in;
		  default:data_out_l128 <= data_in;
		endcase // case (toggle_flag)
   end

   assign data_out_d0 = {data_out_h128, data_out_l128};

   always @(posedge clk_250) begin
      if(~rst_n) begin
         toggle_flag <= 0;
         toggle_flag_d1 <= 0;
      end else begin
         if(data_in_vld) begin
            toggle_flag <= ~toggle_flag;
         end else begin
            toggle_flag <= toggle_flag;
         end
         toggle_flag_d1 <= toggle_flag;
      end
   end // always @ (posedge clk_250)

endmodule // clk250_to_clk125
