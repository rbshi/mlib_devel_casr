//-----------------------------------------------------------------------------
// Title         : Scale 1/4
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : scale_4.v
// Author        : Runbin Shi
// Created       : 21.11.2016
// Last modified : 21.11.2016
//-----------------------------------------------------------------------------
// Description : scale the datainput -> 1/4
// 
//-----------------------------------------------------------------------------

module scale_4(
                   input wire         clk,
                   input wire         rst_n,
                   input wire         data_in_vld,
                   input wire [127:0] data_in,

                   output reg [127:0] data_out,
                   output reg         data_out_vld
                   );

   parameter BITS_PER_PIX = 8;
   
   always @(posedge clk) begin
      case (data_in_vld)
        1: begin
           data_out_vld <= 1'b1;
        end
        default: begin
           data_out_vld <= 1'b0;
        end
      endcase // case (en)
   end // always @ (posedge clk)

   always @(posedge clk) begin
      data_out[1*BITS_PER_PIX-1:0*BITS_PER_PIX] <= data_in[1*BITS_PER_PIX-1:0*BITS_PER_PIX] >> 2;
      data_out[2*BITS_PER_PIX-1:1*BITS_PER_PIX] <= data_in[2*BITS_PER_PIX-1:1*BITS_PER_PIX] >> 2;
      data_out[3*BITS_PER_PIX-1:2*BITS_PER_PIX] <= data_in[3*BITS_PER_PIX-1:2*BITS_PER_PIX] >> 2;
      data_out[4*BITS_PER_PIX-1:3*BITS_PER_PIX] <= data_in[4*BITS_PER_PIX-1:3*BITS_PER_PIX] >> 2;
      data_out[5*BITS_PER_PIX-1:4*BITS_PER_PIX] <= data_in[5*BITS_PER_PIX-1:4*BITS_PER_PIX] >> 2;
      data_out[6*BITS_PER_PIX-1:5*BITS_PER_PIX] <= data_in[6*BITS_PER_PIX-1:5*BITS_PER_PIX] >> 2;
      data_out[7*BITS_PER_PIX-1:6*BITS_PER_PIX] <= data_in[7*BITS_PER_PIX-1:6*BITS_PER_PIX] >> 2;
      data_out[8*BITS_PER_PIX-1:7*BITS_PER_PIX] <= data_in[8*BITS_PER_PIX-1:7*BITS_PER_PIX] >> 2;
      data_out[9*BITS_PER_PIX-1:8*BITS_PER_PIX] <= data_in[9*BITS_PER_PIX-1:8*BITS_PER_PIX] >> 2;
      data_out[10*BITS_PER_PIX-1:9*BITS_PER_PIX] <= data_in[10*BITS_PER_PIX-1:9*BITS_PER_PIX] >> 2;
      data_out[11*BITS_PER_PIX-1:10*BITS_PER_PIX] <= data_in[11*BITS_PER_PIX-1:10*BITS_PER_PIX] >> 2;
      data_out[12*BITS_PER_PIX-1:11*BITS_PER_PIX] <= data_in[12*BITS_PER_PIX-1:11*BITS_PER_PIX] >> 2;
      data_out[13*BITS_PER_PIX-1:12*BITS_PER_PIX] <= data_in[13*BITS_PER_PIX-1:12*BITS_PER_PIX] >> 2;
      data_out[14*BITS_PER_PIX-1:13*BITS_PER_PIX] <= data_in[14*BITS_PER_PIX-1:13*BITS_PER_PIX] >> 2;
      data_out[15*BITS_PER_PIX-1:14*BITS_PER_PIX] <= data_in[15*BITS_PER_PIX-1:14*BITS_PER_PIX] >> 2;
      data_out[16*BITS_PER_PIX-1:15*BITS_PER_PIX] <= data_in[16*BITS_PER_PIX-1:15*BITS_PER_PIX] >> 2;
   end // always @ (posedge clk)

endmodule 


