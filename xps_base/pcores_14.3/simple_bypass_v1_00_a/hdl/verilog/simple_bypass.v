//-----------------------------------------------------------------------------
// Title         : Simple By Pass for Test
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : simple_bypass.v
// Author        : Runbin Shi
// Created       : 23.11.2016
// Last modified : 23.11.2016
//-----------------------------------------------------------------------------
// Description :
//
//-----------------------------------------------------------------------------

module simple_bypass(
                     input wire         clk_125,
                     input wire [127:0] data_in,
                     input wire         data_in_vld,

                     output reg [127:0] data_out,
                     output reg data_out_vld
);

   always @(posedge clk_125) begin
      data_out_vld <= data_in_vld;
      data_out <= data_in;
   end

endmodule // simple_bypass
